package security41.jwt.handler;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import security41.jwt.common.Results;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 未登录  handler
 */
@Slf4j
public class NotLoginHandler implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        log.error("用户未登录,{}", e.getMessage(), e);

        response.setContentType("application/json;charset=UTF-8");
        Results results = new Results();
        results.setCode(401);
        results.setMsg("用户未登录");
        response.getWriter().write(JSON.toJSONString(results));
    }

}

