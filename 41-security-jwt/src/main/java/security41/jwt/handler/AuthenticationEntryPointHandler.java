package security41.jwt.handler;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import security41.jwt.common.Results;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class AuthenticationEntryPointHandler implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        log.error("token错误或者过期,{}", e.getMessage(), e);

        response.setContentType("application/json;charset=UTF-8");
        Results results = new Results();
        results.setCode(401);
        results.setMsg("TOKEN过期或者无效");
        response.getWriter().write(JSON.toJSONString(results));
    }
}
