package security41.jwt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import security41.jwt.common.Results;
import security41.jwt.service.TUserService;

import java.security.Principal;

@RestController
public class TUserController {

    @Autowired
    private TUserService tUserService;

    @GetMapping("/")
    public Results hello(Authentication authentication) {
        Results results = new Results();
        results.setCode(200);
        results.setMsg("SUCCESS");
        results.setData(authentication.getName());
        return results;
    }

    @GetMapping("/hello")
    public Results hello(Principal principal) {
        Results results = new Results();
        results.setCode(200);
        results.setMsg("SUCCESS");
        results.setData(principal.getName());
        return results;
    }
}
