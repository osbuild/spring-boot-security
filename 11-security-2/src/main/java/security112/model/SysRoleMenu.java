package security112.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SysRoleMenu {
    private Long roleId;
    private Long menuId;
}
